# Creating an SFTP User for the website

## Add the user

```
sudo adduser USERNAME
```

## Disable Shell

```
sudo usermod -s /bin/false USERNAME
```

## Enable SFTP Access

Edit `/etc/ssh/sshd_config` e.g. `sudo nano /etc/ssh/sshd_config`

Make sure `Subsystem sftp /usr/lib/openssh/sftp-server` is commented out.

Add at the bottom, changing both the USERNAME and WEBSITE 

```
Subsystem sftp internal-sftp

Match User USERNAME
   ChrootDirectory /home
   ForceCommand internal-sftp -d /forge/WEBSITE
   X11Forwarding no
   AllowTCPForwarding no
   PasswordAuthentication yes
```

## Permissions

By default the user will have read access to all the website files, but cannot overwrite/update them.

To share permissions with the `forge` user we add the user to the forge group

```
sudo usermod -aG forge USERNAME
```

Once they belong to the user group, we must make the files group writable within your desired directory.

Often this might be the whole website directory, or it might be just a folder isolated to them e.g. "my uploads"

```
sudo chmod g+w -R /home/forge/WEBSITE

# or

sudo chmod g+w -R /home/forge/WEBSITE/public/user-uploads
```

To keep file ownership a little more tidy, we can make new files inherit the parent permissions with..

```
sudo chmod g+s -R /home/forge/WEBSITE
```

Once everything is done, restart ssh

```
sudo service ssh restart
```
