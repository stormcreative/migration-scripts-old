# Moving UGC from server to server.

## Creating the backups

Take a database and media backup.

First move into a publicly accessible directory, such as the document root e.g. `/var/www/website-name.com/public`

```
bash -c "$(curl -s https://bitbucket.org/stormcreative/migration-scripts/raw/HEAD/ugc.sh)"
```

This will generate you a database dump and a media directory dump

## Restoring the backups

### Database

Download the .sql.gz file generated, and import manually via SequalPro or similar.

### Media

SSH into the destination server, and CD into the directory you want to extract the media into.

e.g. If the final destination should be `/var/www/website-name.com/public/media` then you should change (create) into it.

Run the download script:

```
bash -c "$(curl -s https://bitbucket.org/stormcreative/migration-scripts/raw/HEAD/media.sh)"
```
