#!/bin/bash

function banner () {
  echo '*********************************'
  echo '** ' $1 ' **'
  echo '*********************************'
}

function say () {
  echo ''
  echo '' $1
  echo ''
}

function line () {
  echo $1
}

banner 'Server Migration Helper'

say 'Taking Database Dump'
line 'Please provide the DB user'
read DBUSER

line 'Please provide the DB name'
read DBNAME

mysqldump -u $DBUSER -p $DBNAME | gzip > $DBNAME.sql.gz
say "Saved to $DBNAME.sql.gz"

say 'Creating Zip of Media'
line 'Please provide the full system path to the media directory'
read DIR

TARNAME="$(basename $DIR).tar.gz"
tar -C $DIR -czf ${TARNAME} ./
say "Saved to $TARNAME"




