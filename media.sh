#!/bin/bash

echo 'Please provide the URL of the tar.gz to restore'
read URL

curl -s $URL > media.tar.gz
tar -xzf media.tar.gz
rm -f media.tar.gz
ls -la
